# -*- coding: utf-8 -*-
"""
Éditeur de Spyder

Ceci est un script temporaire.
"""

class Point():
    
    def __init__(self,x=0,y=0):
        self._x=x
        self._y=y
    
    def x(self):
        return self._x
    
    def y(self):
        return self._y
    
    def __str__(self):
        return '(' + str(self.x()) + ',' + str(self.y()) + ')'
    
    def __eq__(self,b):
        return self.x()==b.x() and self.y()==b.y()

