#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb  3 14:07:01 2020

@author: m16035433
"""

import math
import Point_light as P

class Polygone:
    
    def __init__(self,sommets):
        self.sommets=sommets
        
    def getSommet(self,i):
        return self.sommets[i]

    def aire(self):
        s=0
        for i in range(len(self.sommets)-1):
            s+= (self.sommets[i].x() + self.sommets[i+1].x()) * (self.sommets[i].y() + self.sommets[i+1].y())
        return 0.5*s
        
    def __str__(self):
        r='[ '
        for i in self.sommets:
            r=r+(str(i))+' '
        return r + ']'


class Triangle(Polygone):

    def __init__(self,a,b,c):
        super().__init__([a,b,c])
        #Polygone.__init__(self,[a,b,c])

class Rectangle(Polygone):
    
    def __init__(self,xmin,xmax,ymin,ymax):
        a=P.Point(xmin,ymin)
        b=P.Point(xmax,ymin)
        c=P.Point(xmax,ymax)
        d=P.Point(xmin,ymax)
        self.sommets=[a,b,c,d]
        
class PolygoneRegulier(Polygone):
    
    def __init__(self,centre,rayon,nbsommets):
        self.sommets,xi,yi=[],0,0
        for i in range(nbsommets):
            xi=centre.x()+rayon*math.cos(2*math.pi*i/nbsommets)
            yi=centre.x()+rayon*math.sin(2*math.pi*i/nbsommets)
            self.sommets.append(P.Point(xi,yi))



a=P.Point(0,0)
b=P.Point(1,0)
c=P.Point(1,1)
d=P.Point(0,1)
s=[a,b,c,d]
p=Polygone(s)
print(p.getSommet(2))
print("poly",p)
print(p.aire())
t=Triangle(a,b,d)
print("tri",t)
print(t.aire())
r=Rectangle(0,1,0,1)
print("rect",r)
print(r.aire())
r=PolygoneRegulier(c,1,5)
print("reg",r)
print(r.aire())